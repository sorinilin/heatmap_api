#Heatmap API

- PHP 7.4
- Symfony 5.3
- Postgres 13
---
- Run `composer update` on your local machine from project folder
- Docker database and webserver service from docker-compose.yml:
`docker-compose up -d`
- Run `docker exec -it php74-apache-container php bin/console doctrine:migrations:migrate` to create required table
---
- Heatmap application routes:
    1. localhost:8080/homepage - homepage, categories, checkout pages
    2. localhost:8080/api/heatmap - POST route
    3. localhost:8080/api/customer/{userId} - GET route. Use "customer01" as test userId. To create another userId please change the value of "customerId" in heatmap object inside heatmap.js
    4. localhost:8080/api/link/{link}?date_start=YY-MM-DD hh:mm:ss&date_end=YY-MM-DD hh:mm:ss - GET route, use "homepage", "laptop", "pc", "tv" as test values.
    5. localhost:8080/api/type/{type}?date_start=YY-MM-DD hh:mm:ss&date_end=YY-MM-DD hh:mm:ss - GET route, use "Homepage", "Category", "Checkout" as test values.
