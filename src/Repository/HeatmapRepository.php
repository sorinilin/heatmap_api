<?php

namespace App\Repository;

use App\Entity\Heatmap;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Heatmap|null find($id, $lockMode = null, $lockVersion = null)
 * @method Heatmap|null findOneBy(array $criteria, array $orderBy = null)
 * @method Heatmap[]    findAll()
 * @method Heatmap[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HeatmapRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Heatmap::class);
    }

    /**
     * @param $url
     * @param $dateStart
     * @param $dateEnd
     * @return int|mixed|string
     */
    public function findByUrl($url, $dateStart, $dateEnd)
    {
        $queryBuilder = $this->createQueryBuilder('q');
        return $queryBuilder
            ->andWhere($queryBuilder->expr()->like('q.url', ':url'))
            ->andWhere($queryBuilder->expr()->between('q.timestamp', ':date_start', ':date_end'))
            ->setParameter('url', '%'. $url . '%')
            ->setParameter('date_start', $dateStart)
            ->setParameter('date_end', $dateEnd)
            ->orderBy('q.timestamp', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param $type
     * @param $dateStart
     * @param $dateEnd
     * @return int|mixed|string
     */
    public function findByType($type, $dateStart, $dateEnd)
    {
        $queryBuilder = $this->createQueryBuilder('q');
        return $queryBuilder
            ->andWhere($queryBuilder->expr()->like('q.type', ':type'))
            ->andWhere($queryBuilder->expr()->between('q.timestamp', ':date_start', ':date_end'))
            ->setParameter('type', '%'. $type . '%')
            ->setParameter('date_start', $dateStart)
            ->setParameter('date_end', $dateEnd)
            ->orderBy('q.timestamp', 'ASC')
            ->getQuery()
            ->getResult();
    }

    // /**
    //  * @return Heatmap[] Returns an array of Heatmap objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('h.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Heatmap
    {
        return $this->createQueryBuilder('h')
            ->andWhere('h.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
