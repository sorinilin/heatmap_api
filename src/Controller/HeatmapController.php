<?php

namespace App\Controller;

use App\Entity\Heatmap;
use App\Form\HeatmapType;
use App\Repository\HeatmapRepository;
use App\Serializer\FormErrorSerializer;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HeatmapController extends AbstractController
{
    private $entityManager;
    private $formErrorSerializer;
    private $heatmapRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param FormErrorSerializer $formErrorSerializer
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        FormErrorSerializer $formErrorSerializer,
        HeatmapRepository $heatmapRepository
    )
    {
        $this->entityManager = $entityManager;
        $this->formErrorSerializer = $formErrorSerializer;
        $this->heatmapRepository = $heatmapRepository;
    }

    /**
     * @Route("/api/type/{type}", name="get_type", defaults={"dateStart": "2000-01-01", "dateEnd": "2100-12-31"}, methods={"GET"})
     * @param $type
     * @param $dateStart
     * @param $dateEnd
     * @param Request $request
     * @return JsonResponse
     */
    public function getType($type, $dateStart, $dateEnd, Request $request): JsonResponse
    {
        $this->buildInterval($request, $dateStart, $dateEnd);
        return new JsonResponse(
            [
                "count" => count($this->heatmapRepository->findByType($type, $dateStart, $dateEnd)),
                "links" => $this->heatmapRepository->findByType($type, $dateStart, $dateEnd)
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/api/link/{url}", name="get_link", defaults={"dateStart": "2000-01-01", "dateEnd": "2100-12-31"}, methods={"GET"})
     * @param $url
     * @param $dateStart
     * @param $dateEnd
     * @param Request $request
     * @return JsonResponse
     */
    public function getLink($url, $dateStart, $dateEnd, Request $request): JsonResponse
    {
        $interval = $this->buildInterval($request, $dateStart, $dateEnd);
        return new JsonResponse(
            [
                "count" => count($this->heatmapRepository->findByUrl($url, $interval[0], $interval[1])),
                "links" => $this->heatmapRepository->findByUrl($url, $interval[0], $interval[1])
            ],
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/api/customer/{customerId}", name="get_customer", methods={"GET"})
     * @param $customerId
     * @return JsonResponse
     */
    public function getCustomer($customerId): JsonResponse
    {
        $heatmap = $this->heatmapRepository->findBy([
            "customerId" => $customerId
        ]);
        return new JsonResponse(
            $heatmap,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/api/heatmap", name="post_heatmap", methods={"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function post(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $form = $this->createForm(HeatmapType::class, new Heatmap());
        $form->submit($data);

        if (false === $form->isValid()) {
            return new JsonResponse([
                'status' => 'error',
                'errors' =>$this->formErrorSerializer->convertFormToArray($form)
            ], JsonResponse::HTTP_BAD_REQUEST
            );
        }
        $this->entityManager->persist($form->getData());
        $this->entityManager->flush();

        return new JsonResponse([
            'status' => 'ok',
        ], JsonResponse::HTTP_CREATED
        );
    }

    /**
     * @param Request $request
     * @param $dateStart
     * @param $dateEnd
     * @return array
     */
    private function buildInterval(Request $request, $dateStart, $dateEnd): array
    {
        if ($request->query->get('date_start')) {
            $dateStart = $request->query->get('date_start');
        }
        if ($request->query->get('date_end')) {
            $dateEnd = $request->query->get('date_end');
        }
        return [$dateStart, $dateEnd];
    }
}
