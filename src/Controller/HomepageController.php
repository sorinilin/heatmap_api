<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomepageController extends AbstractController
{
    /**
     * @Route("/homepage", name="homepage")
     */
    public function index(): Response
    {
        return $this->render('homepage/index.html.twig', [
            'controller_name' => 'HomepageController',
        ]);
    }

    /**
     * @Route("/laptop", name="laptop_category")
     */
    public function laptopCategory(): Response
    {
        return  $this->render('homepage/laptop.html.twig', [
            'controller_name' => 'HomepageController'
        ]);
    }

    /**
     * @Route("/pc", name="pc_category")
     */
    public function pcCategory(): Response
    {
        return  $this->render('homepage/pc.html.twig', [
            'controller_name' => 'HomepageController'
        ]);
    }

    /**
     * @Route("/tv", name="tv_category")
     */
    public function tvCategory(): Response
    {
        return  $this->render('homepage/tv.html.twig', [
            'controller_name' => 'HomepageController'
        ]);
    }

    /**
     * @Route("/checkout", name="checkout")
     */
    public function checkout(): Response
    {
        return $this->render('homepage/checkout.html.twig', [
            'controller_name' => 'HomepageController'
        ]);
    }
}
