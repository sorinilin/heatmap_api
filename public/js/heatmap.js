console.log('Hello heatmap!');
$(document).ready(function() {
    const heatmap = {
        "url": window.location.href,
        "type": document.title,
        "timestamp": document.lastModified,
        "customerId": "customer01"
    };
    console.log(heatmap);
    $.ajax({
        url: 'http://localhost:8080/api/heatmap',
        dataType: 'json',
        type: 'post',
        contentType: 'application/json',
        data: JSON.stringify({"url": heatmap.url, "type": heatmap.type, "timestamp": heatmap.timestamp, "customerId": heatmap.customerId}),
        processData: false,
        success: function(data, status, jqXHR) {
            console.log(status, data);
        },
        error: function(jqXHR, status, error) {
            console.log(status, error);

        }
    });
});
